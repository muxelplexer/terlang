cmake_minimum_required(VERSION 3.15)

project(
    terlang-c
    VERSION 0.0.1
    DESCRIPTION "Reference terlang compiler implementation in C"
)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

find_program(CCACHE_PROGRAM ccache)
if(CCACHE_PROGRAM)
    set(CMAKE_CXX_COMPILER_LAUNCHER "${CCACHE_PROGRAM}")
    set(CMAKE_CUDA_COMPILER_LAUNCHER "${CCACHE_PROGRAM}") # CMake 3.9+
endif()


add_executable(${PROJECT_NAME})

add_subdirectory(src)